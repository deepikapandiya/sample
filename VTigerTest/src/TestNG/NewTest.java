package TestNG;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class NewTest {
	public WebDriver browser;

	@BeforeTest

	public void beforeTest() {

		System.setProperty("webdriver.chrome.driver", "F:\\testing\\JAR files\\chromedriver.exe");
		WebDriver browser = new ChromeDriver();

		browser.manage().window().maximize();

		browser.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		browser.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		browser.get("http://localhost:8888/");

	}

	@Test(priority = 1)
	public void login(){

		
				browser.findElement(By.id("username")).sendKeys("admin");
		
		

		WebElement passwordtextbox = browser.findElement(By.xpath

		("//input[@name ='password']"));
		passwordtextbox.sendKeys("admin");
		

		WebElement signinbutton = browser.findElement

		(By.xpath("//*[@id =\"forgotPassword\"]/button"));
		signinbutton.click();
		

	}

	@Test(priority = 2)
  public void addproduct() 
  {
	  
	  
	    browser.findElement(By.linkText("Products")).click();
	  
		browser.findElement(By.xpath("//*[@id='Products_listView_basicAction_LBL_ADD_RECORD']/strong")).click();
		
		browser.findElement(By.id

("Products_editView_fieldName_productname")).sendKeys("moblie");
		browser.findElement(By.xpath("//*[@id='Products_editView_fieldName_sales_start_date']")).sendKeys("02-05-2017");
		
		//browser.findElement(By.xpath("//*[@id='EditView']/div[2]/div[1]/button/strong")).click();
		
		browser.findElement(By.xpath("//*[@class='btn btn-success']")).click();
		System.out.println("save contact"); 
		
  }

	@Test(priority =3)
  
  public void signout() 
  {
	  
	  
	  browser.findElement(By.xpath(".//*[@id='menubar_item_right_Administrator']/strong")).click();
	  browser.findElement(By.xpath(".//*[@id='menubar_item_right_LBL_SIGN_OUT']")).click();System.out.println("Logout completed");
	  
  }

	@AfterTest

	public void afterTest() {

		browser.quit();
		System.out.println("close browser");

	}

}
